#!/usr/bin/env node

const fs = require('fs')
const ipv6 = require('ipv6')
const Twit = require('twit')
const async = require('async')
const phantom = require('phantom')
const minimist = require('minimist')
const Mastodon = require('mastodon')
const Mustache = require('mustache')
const {WikiChanges} = require('wikichanges')
const iplocation = require("iplocation").default;
const getCountryForCode = require("./country_code").getCoutryForCode
const sqlite3 = require('sqlite3').verbose();

const argv = minimist(process.argv.slice(2), {
    default: {
        verbose: false,
        config: './config.json'
    }
})

function getConfig(path) {
    const config = loadJson(path)
    // see if ranges are externally referenced as a separate .json files
    if (config.accounts) {
        for (let account of Array.from(config.accounts)) {
            if (typeof account.ranges === 'string') {
                account.ranges = loadJson(account.ranges)
            }
        }
    }
    console.log("loaded config from", path)
    return config
}

function loadJson(path) {
    if ((path[0] !== '/') && (path.slice(0, 2) !== './')) {
        path = `./${path}`
    }
    return require(path)
}


async function getCountry(name) {
    return await new Promise((resolve, reject) => {
        iplocation(name, [], (error, res) => {
            if (error)
                reject(error);
            let location = getCountryForCode(res.country)
            let status = {
                name: name,
                location: location
            }
            resolve(status);
        })
    })


}

async function inspect(account, edit) {
    if (edit.url) {
        if (edit.anonymous) {
            let name = edit.user
            let status = await getCountry(name)
            let date = new Date();
            let sqllite_date = date.toISOString().slice(0,10).replace('-','').replace('-','');
            var ins = 'INSERT OR IGNORE INTO absCount (Day, Country) VALUES ('+sqllite_date+',\''+ status.location+'\')'
            db.run(ins)
            var x = 'UPDATE absCount Set count = count + 1 WHERE Day = '+sqllite_date+' AND Country = \''+status.location +'\''
            db.run(x)
            //db.run('INSERT INTO absCount(Day, Country) VALUES('+sqllite_date+','+ status.location +') ON CONFLICT(Day, Country) DO UPDATE absCount SET count = 0 WHERE Day ='+sqllite_date+ 'AND Country ='+ status.location)

            console.log(status)
            //sendStatus(account, status, edit)
        }
    }
}

function makebarspickture() {
    var data = [
        {
            x: ['giraffes', 'orangutans', 'monkeys'],
            y: [20, 14, 23],
            type: 'bar'
        }
    ];
    layout = {title: "Simple Javascript Graph"};
    Plotly.newPlot('myDiv', data, layout).then(
        function (gd) {
            Plotly.toImage(gd, {height: 300, width: 300})
                .then(
                    function (url) {
                        img_jpg.attr("src", url);
                        return Plotly.toImage(gd, {format: 'jpeg', height: 400, width: 400});
                    }
                )
        });
}

function sendStatus(account, status, edit) {
    console.log(status)

    if (!argv.noop && (!account.throttle || !isRepeat(edit))) {

        makebarspicture().then(function (screenshot) {

            // Twitter
            if (account.access_token) {
                const twitter = new Twit(account)
                const b64content = fs.readFileSync(screenshot, {encoding: 'base64'})

                // upload the screenshot to twitter
                twitter.post('media/upload', {media_data: b64content}, function (err, data, response) {
                    if (err) {
                        console.log(err)
                        return
                    }

                    // add alt text for the media, for use by screen readers
                    const mediaIdStr = data.media_id_string
                    const altText = "Screenshot of edit to " + edit.page
                    const metaParams = {media_id: mediaIdStr, alt_text: {text: altText}}

                    twitter.post('media/metadata/create', metaParams, function (err, data, response) {
                        if (err) {
                            console.log('metadata upload for twitter screenshot alt text failed with error', err)
                        }
                        const params = {
                            'status': status,
                            'media_ids': [mediaIdStr]
                        }
                        twitter.post('statuses/update', params, function (err) {
                            if (err) {
                                console.log(err)
                            }
                        })
                        fs.unlink(screenshot)
                    })
                })
            }

        })
    }
}

function canTweet(account, error) {
    if (!account.access_token) {
        error(null)
    } else {
        try {
            const twitter = new Twit(account)
            const a = account['access_token']
            return twitter.get('search/tweets', {q: 'cats'}, function (err, data, response) {
                if (err) {
                    error(err + " for access_token " + a)
                } else if (!response.headers['x-access-level'] ||
                    (response.headers['x-access-level'].substring(0, 10) !== 'read-write')) {
                    error(`no read-write permission for access token ${a}`)
                } else {
                    error(null)
                }
            })
        } catch (err) {
            error(`unable to create twitter client for account: ${account}`)
        }
    }
}

function dbconnect(){
    db = new sqlite3.Database('./Database.db', (error, db) => {
        if(error){
            console.error(error.message);
        }
        console.log('Connected to Database')
    });
    db.run('CREATE TABLE IF NOT EXISTS absCount(Day date NOT NULL, Country VARCHAR NOT NULL, count DOUBLE DEFAULT 0, PRIMARY KEY(Country,Day))')
}

function main() {
    dbconnect()
    const config = getConfig(argv.config) //get Config from standart config or args
    const wikipedia = new WikiChanges({ircNickname: config.nick, wikipedias: ["#de.wikipedia"]})
    return wikipedia.listen(edit => {
        if (argv.verbose) {
            console.log(JSON.stringify(edit))
        }
        Array.from(config.accounts).map((account) =>
            inspect(account, edit))
    })
}


if (require.main === module) {
    main()
}

module.exports = {
    main,
    getConfig,
    getStatus: getCountry,
    makebarspickture,
    iplocation,
    inspect
}
