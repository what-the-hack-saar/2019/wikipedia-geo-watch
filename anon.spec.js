const anon = require('./anon')


const config = anon.getConfig('./config.json')


describe("basis integration test", () => {
    it("should successfully parse an irc entry", async () => {
        const edit = { "channel": "#de.wikipedia", "flag": "!N", "page": "Information Processing Technology Office", "pageUrl": "http://de.wikipedia.org/wiki/Information_Processing_Technology_Office", "url": "https://de.wikipedia.org/w/index.php?oldid=184683324&rcid=268788145", "delta": 3, "comment": "[[Hilfe:Zusammenfassung und Quellen#Auto-Zusammenfassung|AZ]]: Die Seite wurde neu angelegt: lol", "wikipedia": "German Wikipedia", "wikipediaUrl": "http://de.wikipedia.org", "wikipediaShort": "de", "wikipediaLong": "German Wikipedia", "user": "93.193.187.161", "userUrl": "http://de.wikipedia.org/wiki/User:93.193.187.161", "unpatrolled": true, "newPage": true, "robot": false, "anonymous": true, "namespace": "article" }



        let result = await Promise.all(Array.from(config.accounts).map(async (account) =>
            await anon.inspect(account, edit)
            ))
        console.log(result)
    })
})